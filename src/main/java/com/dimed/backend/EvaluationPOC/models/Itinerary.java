package com.dimed.backend.EvaluationPOC.models;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.dimed.backend.EvaluationPOC.dto.ItineraryDTO;

@Document
public class Itinerary {

	@Id
	private String idlinha;
	private String nome;
	private String codigo;
	private boolean active;
	
	private Map<Integer ,Localization> coordinates;
	
	public Itinerary(String idlinha, String nome, String codigo, Map<Integer, Localization> map) {
		this.coordinates = map;
		this.idlinha = idlinha;
		this.nome = nome;
		this.codigo = codigo;
	}
	
	public Itinerary(ItineraryDTO dto) {
		this.idlinha = dto.getIdlinha();
		this.nome = dto.getNome();
		this.codigo = dto.getCodigo();
		this.coordinates = dto.getCoordinates();
		this.active = true;
	}
	
	public Itinerary() {	}
	
	public String getIdlinha() {
		return idlinha;
	}
	public void setIdlinha(String idlinha) {
		this.idlinha = idlinha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public Map<Integer, Localization> getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(Map<Integer, Localization> coordinates) {
		this.coordinates = coordinates;
	}
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "Itinerary [idlinha=" + idlinha + ", nome=" + nome + ", codigo=" + codigo+ "]";
	}		
}
