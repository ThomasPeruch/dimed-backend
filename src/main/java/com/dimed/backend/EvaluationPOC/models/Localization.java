package com.dimed.backend.EvaluationPOC.models;

public class Localization {
	private Double lat;
	private Double lng;
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLng() {
		return lng;
	}
	public void setLng(Double lng) {
		this.lng = lng;
	}
	
	public Localization() {
		super();
	}
	public Localization(Double lat, Double lng) {
		super();
		this.lat = lat;
		this.lng = lng;
	}
	@Override
	public String toString() {
		return "Localization [lat=" + lat + ", lng=" + lng + "]";
	}
	
	
	
	

}
