package com.dimed.backend.EvaluationPOC.models;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class BusLine implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	private String id;
	private String codigo;
	private String nome;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BusLine(String id, String codigo, String nome) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
	} 
	
	public BusLine() {}
	@Override
	public String toString() {
		return "BusLine [id=" + id + ", codigo=" + codigo + ", nome=" + nome + "]";
	}
}
