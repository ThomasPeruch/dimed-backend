package com.dimed.backend.EvaluationPOC.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dimed.backend.EvaluationPOC.dto.ItineraryDTO;
import com.dimed.backend.EvaluationPOC.dto.ShortItineraryDTO;
import com.dimed.backend.EvaluationPOC.models.BusLine;
import com.dimed.backend.EvaluationPOC.models.Itinerary;
import com.dimed.backend.EvaluationPOC.models.Localization;
import com.dimed.backend.EvaluationPOC.repositories.ItineraryRepository;
import com.dimed.backend.EvaluationPOC.services.exceptions.InvalidArgumentException;
import com.dimed.backend.EvaluationPOC.services.exceptions.NotFoundError;
import com.dimed.backend.EvaluationPOC.utils.Calculus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class ItineraryService {
	
	private RestTemplate restTemplate;
	private String urlBusLineItinerary;
	
	@Autowired
	private ItineraryRepository repository;
	
	public ItineraryService() {
		this.restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
	    converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
	    restTemplate.getMessageConverters().add(converter);
		this.urlBusLineItinerary = "http://www.poatransporte.com.br/php/facades/process.php?a=il&p=";	
	}
	
	public ItineraryDTO getItineraryById(String idlinha) throws JsonMappingException, JsonProcessingException {	
		Optional<Itinerary>opt = getItineraryByIdIntoRepository(idlinha);
		if(opt.isEmpty()) {
			ItineraryDTO dto = getItineraryByIdInApi(idlinha);
			dto.setIdlinha(idlinha);
			repository.save(new Itinerary(dto));
			return dto;
		}else {			
			Itinerary itinerary =opt.get();
			if(itinerary.isActive()) {
				return new ItineraryDTO(itinerary);		
			}else {
				throw new NotFoundError("id nao encontrado");
			}
		}
	}

	private Optional<Itinerary> getItineraryByIdIntoRepository(String idlinha) {
		return repository.findById(idlinha);
	}
	
	private ItineraryDTO getItineraryByIdInApi (String idlinha) throws JsonMappingException, JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		ResponseEntity<String> response = restTemplate.getForEntity(urlBusLineItinerary+idlinha, String.class);
		ItineraryDTO dto = new ItineraryDTO ();
		
		JsonNode node = mapper.readTree(response.getBody());		
		dto.setNome(node.path("nome").asText());
		dto.setCodigo(node.path("codigo").asText());
		dto.setIdlinha(node.path("idlinha").asText());
		Integer position = 0 ;
		Map<Integer, Localization> map= new HashMap<Integer, Localization>();
		
		do{
			JsonNode coordinates = node.path(position.toString());		
			if(coordinates.isMissingNode()) {
				break;
			}
			Double lat = coordinates.path("lat").asDouble();
			Double lng = coordinates.path("lng").asDouble();				
			Localization local = new Localization(lat,lng);				
			map.put(position, local);
			position++;
		}while(true);
		dto.setCoordinates(map);	
		
	return dto;
}

	public List<ShortItineraryDTO> getItineraryByRadius(Double lat, Double longit, Double radius,List<BusLine> apiList) throws InterruptedException, JsonMappingException, JsonProcessingException {		
			List<ShortItineraryDTO> list = new ArrayList<>();
			for (BusLine line : apiList) {
				Thread.sleep(100);
				ItineraryDTO dto= getItineraryById(line.getId());
				for (Entry<Integer, Localization> coord : dto.getCoordinates().entrySet()) {
					if(Calculus.distanceCalculation(lat, coord.getValue().getLat(), longit, coord.getValue().getLng()) <= radius) {						
						list.add(new ShortItineraryDTO(dto));
					}
				}
			}
			return list;
	}	

	public ItineraryDTO save(ItineraryDTO dto) {
		Itinerary entity = new Itinerary();
		Optional<Itinerary> opt = getItineraryByIdIntoRepository(dto.getIdlinha());
		if(opt.isEmpty()) {
			if(existsIdInApi(dto.getIdlinha())) {
				throw new InvalidArgumentException("Esse id ja existe!");				
			}else {
				copyDtoToEntity(dto, entity);
				entity.setActive(true);
				repository.save(entity);
				return dto;
			}
		}else {
			throw new InvalidArgumentException("Esse id ja existe!");
		}
	}

	private boolean existsIdInApi (String idlinha) {
		ResponseEntity<String> response = restTemplate.getForEntity(urlBusLineItinerary+idlinha, String.class);
		if(response.getBody().contains("encontrada")) {
			return false;
		}
		return true;
	}
	
	public ItineraryDTO update (ItineraryDTO dto, String idlinha) throws JsonMappingException, JsonProcessingException {
		Optional<Itinerary>opt = getItineraryByIdIntoRepository(idlinha);
		Itinerary entity;
		if(opt.isEmpty()) {
			if(existsIdInApi(idlinha)) {
				entity = buildItinerary(dto);
				entity.setIdlinha(idlinha);
				entity.setActive(true);
				repository.save(entity);
				dto.setIdlinha(idlinha);
				return dto;
			}else {
				throw new InvalidArgumentException("Esse id nao existe");
			}
		}else {			
			entity = buildItinerary(dto);
			entity.setIdlinha(idlinha);
			entity.setActive(true);
			repository.save(entity);
			return new ItineraryDTO(entity);			
		}
	}
	
	public void delete(String idlinha) throws JsonMappingException, JsonProcessingException {
		Optional<Itinerary>opt = getItineraryByIdIntoRepository(idlinha);
		Itinerary entity = new Itinerary();
		if(opt.isEmpty()) {
			if(existsIdInApi(idlinha)) {
				entity = buildItinerary(getItineraryByIdInApi(idlinha));
			}else {
				throw new NotFoundError("id nao encontrado");				
			}	
		}else {
			entity = opt.get();	
		}
		entity.setActive(false);
		repository.save(entity);
	}
	
	
	private void copyDtoToEntity(ItineraryDTO dto, Itinerary entity) {
		entity.setCoordinates(dto.getCoordinates());
		entity.setIdlinha(dto.getIdlinha());
		entity.setCodigo(dto.getCodigo());
		entity.setNome(dto.getNome());
	}	
	
	private Itinerary buildItinerary(ItineraryDTO requestDto) {
		Itinerary entity = new Itinerary();
		entity.setIdlinha(requestDto.getIdlinha());
		entity.setCodigo(requestDto.getCodigo());
		entity.setNome(requestDto.getNome());
		entity.setCoordinates(requestDto.getCoordinates());
		return entity;
	}
}
