package com.dimed.backend.EvaluationPOC.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dimed.backend.EvaluationPOC.dto.BusLineDTO;
import com.dimed.backend.EvaluationPOC.models.BusLine;
import com.dimed.backend.EvaluationPOC.repositories.BusLineRepository;
import com.dimed.backend.EvaluationPOC.services.exceptions.InvalidArgumentException;

@Service
public class BusLineService {
	
	@Autowired
	private BusLineRepository repository ;
	
	@Transactional(readOnly = true)
	public List<BusLine> findAll(){
		return repository.findAll();	
	}
	
	@Transactional(readOnly = true)
	public List<BusLineDTO> findAllLinesApiIntegrated(List<BusLine> apiList){
		
		List <BusLine>list = new ArrayList<>();
		list.addAll(apiList);
		for (BusLine busLine : findAll()) {
			list.add(busLine);
		}
		List<BusLineDTO>dtoList = new ArrayList<>();
		for (BusLine busLine : list) {
			dtoList.add(new BusLineDTO(busLine));			
		}
		return dtoList;
	}
	
	@Transactional(readOnly = true)
	public List<BusLineDTO> findByName(String nome, List<BusLine> apiList) {	
		List<BusLineDTO> allLines = findAllLinesApiIntegrated(apiList);
		List<BusLineDTO> returnList = new ArrayList<>();
		for (BusLineDTO dto: allLines) {
			if(dto.getNome().contains(nome.toUpperCase())){
				returnList.add(dto);
			}
		}
		return returnList;
	}
	
	@Transactional(readOnly = true)
	public BusLineDTO findById(String id) {
		 Optional<BusLine>opt = repository.findById(id);
		 BusLine entity = opt.orElseThrow(() -> new InvalidArgumentException("N�o foi possivel achar o essa linha de onibus na base de dados"));
		 return new BusLineDTO (entity);
	}
	
	@Transactional
	public BusLineDTO save(BusLineDTO busLineDto, List<BusLine> apiList) {
		for (BusLine line : apiList) {
			if(line.getId().equals(busLineDto.getId())) {
				throw new InvalidArgumentException("Esse id("+busLineDto.getId()+") ja existe, tente usar outro!");
			}
		}
		BusLine entity = new BusLine();
		for (BusLine busLine :  repository.findAll()) {
			if(busLine.getId().equals(busLineDto.getId())) {
				throw new InvalidArgumentException("Esse id("+busLineDto.getId()+") ja existe, tente usar outro!");
			}
		}
		copyDtoToEntity(busLineDto ,entity);
		entity = repository.save(entity);	
		return new BusLineDTO (entity);
	}
	
	@Transactional
	public BusLineDTO update(String id, BusLineDTO dto, List<BusLine> apiList) {
		dto.setId(id);
			for (BusLine busLine : apiList) {
				if(busLine.getId().equals(dto.getId())) {
					busLine.setCodigo(dto.getCodigo());
					busLine.setNome(dto.getNome());
				}	
			}
			for (BusLine busLine : repository.findAll()) {
				if(busLine.getId().equals(dto.getId())) {
					busLine.setCodigo(dto.getCodigo());
					busLine.setNome(dto.getNome());
					busLine.setId(dto.getId());
					repository.save(busLine);				
				}	
			}	
			if(dto.getId() == null) {
				throw new InvalidArgumentException("Esse id ("+id+") n�o existe");
			};
		return dto;	
	}
	
	
	public void delete(String id, List<BusLine> apiList) {
		boolean founded = false;
		BusLine line = new BusLine();
			for (BusLine busLine : apiList) {
				if(busLine.getId().equals(id)) {
					founded=true;
					line = busLine;
				}
			}
			if(line.getId()!=null) {
				apiList.remove(line);
			}
			
			
		for (BusLine busLine : findAll()) {
				if(busLine.getId().equals(id)) {
					founded=true;
					repository.deleteById(busLine.getId());
				}
			}
		if(!founded) {
			throw new InvalidArgumentException("Esse id ("+id+") n�o existe");
		}
	}

	private void copyDtoToEntity(BusLineDTO dto, BusLine entity) {
		entity.setId(dto.getId());
		entity.setCodigo(dto.getCodigo());
		entity.setNome(dto.getNome());
	}	
}
