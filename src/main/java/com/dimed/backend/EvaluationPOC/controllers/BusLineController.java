package com.dimed.backend.EvaluationPOC.controllers;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dimed.backend.EvaluationPOC.dto.BusLineDTO;
import com.dimed.backend.EvaluationPOC.models.BusLine;
import com.dimed.backend.EvaluationPOC.services.BusLineService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/buslines")
public class BusLineController {

	private RestTemplate restTemplate;
	private String urlAllBusLines;
	
	@Autowired
	private BusLineService service;
	
	private List<BusLine> externalList;
	
	public BusLineController() {
		this.restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
	    converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
	    restTemplate.getMessageConverters().add(converter);
		this.urlAllBusLines = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";
		externalList = getAllBusLinesFromExternalAPI();
	}
	
	@GetMapping
	@ApiOperation(value = "Lista todas linhas de onibus, para filtrar as linhas de onibus por nome adicione um parametro de requisicao com o nome.")
	public ResponseEntity<List<BusLineDTO>> getBusLines(@RequestParam(name ="nome") Optional <String> nome) {
		if(nome.isEmpty()) {
			return getAllBusLines();
		}else {
			return getBusLineByName(nome.get());
		}		
	}
	
	private ResponseEntity <List<BusLineDTO>> getBusLineByName(String nome) {
		List<BusLineDTO> dtoList = service.findByName(nome, externalList);
		return ResponseEntity.ok(dtoList);
	}

	@PostMapping
	@ApiOperation(value = "Cria linhas de onibus")
	public ResponseEntity<BusLineDTO>insertBusLine(@RequestBody BusLineDTO busLineDto){
		busLineDto= service.save(busLineDto, externalList);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(busLineDto.getId()).toUri();
		return ResponseEntity.created(uri).body(busLineDto);
	}
	
	@PutMapping(value="/{id}")
	@ApiOperation(value = "Atualiza linhas de onibus conforme o Id passado como variavel de rota")
	public ResponseEntity<BusLineDTO>updateBusLine(@PathVariable(value="id") String id ,@RequestBody BusLineDTO busLineDto){
		busLineDto = service.update(id, busLineDto, externalList);
		return ResponseEntity.ok(busLineDto);
	}
	
	@DeleteMapping(value="/{id}")
	@ApiOperation(value = "Deleta linhas de onibus conforme id passado como variavel de rota")
	public ResponseEntity<BusLineDTO>deleteBusLine(@PathVariable(value="id") String id){
		service.delete(id, externalList);
		return ResponseEntity.noContent().build();
	}
	
	private  List<BusLine> getAllBusLinesFromExternalAPI() {
			ResponseEntity<List<BusLine>> response = restTemplate.exchange(urlAllBusLines, HttpMethod.GET, null, new ParameterizedTypeReference<List<BusLine>>() {});
		return response.getBody();		
	}

	private ResponseEntity<List<BusLineDTO>> getAllBusLines() {
		List<BusLineDTO> list = service.findAllLinesApiIntegrated(externalList);
		return ResponseEntity.ok(list);
	}	
}

