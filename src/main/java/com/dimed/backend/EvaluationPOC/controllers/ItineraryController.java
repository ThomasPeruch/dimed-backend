package com.dimed.backend.EvaluationPOC.controllers;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.dimed.backend.EvaluationPOC.dto.ItineraryDTO;
import com.dimed.backend.EvaluationPOC.dto.ShortItineraryDTO;
import com.dimed.backend.EvaluationPOC.models.BusLine;
import com.dimed.backend.EvaluationPOC.services.ItineraryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/itineraries")
public class ItineraryController {
	
	private RestTemplate restTemplate;
	private List<BusLine> externalList;
	
	
	@Autowired
	private ItineraryService service;
	
	public ItineraryController() {
		this.restTemplate = new RestTemplate();
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
	    converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));
	    restTemplate.getMessageConverters().add(converter);
	}
	
	@GetMapping(value="/{id}")
	@ApiOperation(value="Recupera itinerario por id")
	public ResponseEntity<ItineraryDTO> getItineraryById(@PathVariable("id") String idlinha) throws JsonMappingException, JsonProcessingException {		
		ItineraryDTO dto = new ItineraryDTO();
		dto = service.getItineraryById(idlinha);
		return ResponseEntity.ok(dto);
	}
	
	@PostMapping
	@ApiOperation(value="Cria itinerario")
	public ResponseEntity<ItineraryDTO> insertItinerary(@RequestBody ItineraryDTO itineraryDto) throws JsonMappingException, JsonProcessingException{
		return ResponseEntity.ok(service.save(itineraryDto));
	}
	
	@GetMapping
	@ApiOperation(value="Pega todas linhas que passam em um determinado raio, esse raio � definido por tres valores passados por parametros: lat (latitude), longit (longitude), radius (raio)")
	public ResponseEntity<List<ShortItineraryDTO>>  getItineraryByRadius(@RequestParam Double lat, @RequestParam Double longit, @RequestParam Double radius) throws InterruptedException, JsonMappingException, JsonProcessingException {
		List<ShortItineraryDTO>listDto = service.getItineraryByRadius(lat, longit, radius, externalList);
		return ResponseEntity.ok(listDto);
	}
	
	@PutMapping(value="/{idlinha}")
	@ApiOperation(value="Atualiza um itinerario conforme Id passado como variavel de rota")
	private  ResponseEntity<ItineraryDTO> update(@PathVariable String idlinha, @RequestBody ItineraryDTO dto) throws JsonMappingException, JsonProcessingException {
		return ResponseEntity.ok(service.update(dto, idlinha));		
	}
	
	@DeleteMapping(value="/{idlinha}")
	@ApiOperation(value="Deleta um itinerario conforme Id passado como variavel de rota")
	private  ResponseEntity <ItineraryDTO>delete(@PathVariable String idlinha) throws JsonMappingException, JsonProcessingException {
		service.delete(idlinha);		
		return ResponseEntity.noContent().build();
	}
}
