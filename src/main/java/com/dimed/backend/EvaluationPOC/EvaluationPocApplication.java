package com.dimed.backend.EvaluationPOC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvaluationPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvaluationPocApplication.class, args);
	}

}
