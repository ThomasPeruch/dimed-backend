package com.dimed.backend.EvaluationPOC.utils;

public class Calculus {

	public static double distanceCalculation(double lat, double otherLat, double longit, double otherLongit) {
	
		final int R = 6371;
		
		double latDistance = Math.toRadians(otherLat - lat);
		double longitDistance = Math.toRadians(otherLongit - longit);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(otherLat))	* Math.sin(longitDistance / 2) * Math.sin(longitDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000;
		
		return Math.sqrt(distance);
		
	}
}
