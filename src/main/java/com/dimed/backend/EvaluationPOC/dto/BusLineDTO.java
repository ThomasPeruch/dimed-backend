package com.dimed.backend.EvaluationPOC.dto;

import java.io.Serializable;

import com.dimed.backend.EvaluationPOC.models.BusLine;

public class BusLineDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	private String codigo;
	private String nome;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public BusLineDTO(String id, String codigo, String nome) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
	} 
	
	public BusLineDTO(BusLine busLine) {
		super();
		this.id = busLine.getId();
		this.codigo = busLine.getCodigo();
		this.nome = busLine.getNome();
	}
	
	
	public BusLineDTO() {}
	
	@Override
	public String toString() {
		return "BusLine [id=" + id + ", codigo=" + codigo + ", nome=" + nome + "]";
	}
}
