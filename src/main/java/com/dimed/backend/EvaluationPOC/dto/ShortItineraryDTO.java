package com.dimed.backend.EvaluationPOC.dto;

import com.dimed.backend.EvaluationPOC.models.Itinerary;

public class ShortItineraryDTO {

	private String idlinha;
	private String nome;
	private String codigo;
	
	
	public String getIdlinha() {
		return idlinha;
	}
	public void setIdlinha(String idlinha) {
		this.idlinha = idlinha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public ShortItineraryDTO(Itinerary itinerary) {
		super();
		this.idlinha = itinerary.getIdlinha();
		this.nome =itinerary.getNome();
		this.codigo = itinerary.getCodigo();
	}
	
	public ShortItineraryDTO(ItineraryDTO itineraryDto) {
		super();
		this.idlinha = itineraryDto.getIdlinha();
		this.nome =itineraryDto.getNome();
		this.codigo = itineraryDto.getCodigo();
	}

	public ShortItineraryDTO(String idlinha, String nome, String codigo) {
		super();
		this.idlinha = idlinha;
		this.nome = nome;
		this.codigo = codigo;
	}
	public ShortItineraryDTO() {
		super();
	}
	@Override
	public String toString() {
		return "Itinerary [idlinha=" + idlinha + ", nome=" + nome + ", codigo=" + codigo+ "]";
	}	
}
