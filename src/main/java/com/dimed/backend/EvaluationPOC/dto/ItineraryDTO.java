package com.dimed.backend.EvaluationPOC.dto;

import java.util.Map;

import com.dimed.backend.EvaluationPOC.models.Itinerary;
import com.dimed.backend.EvaluationPOC.models.Localization;

public class ItineraryDTO {

	private String idlinha;
	private String nome;
	private String codigo;
	
	private Map<Integer ,Localization> coordinates;
	
	public String getIdlinha() {
		return idlinha;
	}
	public void setIdlinha(String idlinha) {
		this.idlinha = idlinha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public Map<Integer, Localization> getCoordinates() {
		return coordinates;
	}
	public void setCoordinates(Map<Integer, Localization> coordinates) {
		this.coordinates = coordinates;
	}
	public ItineraryDTO(Itinerary itinerary) {
		super();
		this.coordinates = itinerary.getCoordinates();
		this.idlinha = itinerary.getIdlinha();
		this.nome =itinerary.getNome();
		this.codigo = itinerary.getCodigo();
	}

	public ItineraryDTO(String idlinha, String nome, String codigo, Map<Integer, Localization> map) {
		super();
		this.idlinha = idlinha;
		this.nome = nome;
		this.codigo = codigo;
	}
	public ItineraryDTO() {
		super();
	}
	@Override
	public String toString() {
		return "Itinerary [idlinha=" + idlinha + ", nome=" + nome + ", codigo=" + codigo+ "]";
	}	
}
