package com.dimed.backend.EvaluationPOC.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.dimed.backend.EvaluationPOC.models.Itinerary;

@Repository
public interface ItineraryRepository extends MongoRepository<Itinerary, String>{
}
