package com.dimed.backend.EvaluationPOC.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.dimed.backend.EvaluationPOC.models.BusLine;

@Repository
public interface BusLineRepository extends MongoRepository<BusLine, String>{
	
	public List<BusLine> findByNome(String nome);

}
