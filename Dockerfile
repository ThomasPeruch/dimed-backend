FROM adoptopenjdk/openjdk11:jdk-11.0.6_10-alpine-slim
WORKDIR /poc/dimed/backend
COPY ./build/libs/* ./app.jar
EXPOSE 8080
CMD ["java","-jar","app.jar"]