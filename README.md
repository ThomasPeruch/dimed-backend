Comandos para utilizar a aplicacao( em ordem ) :
    - gradle build
    - docker build -f Dockerfile -t backend-dimed .
    - docker-compose up

Foi escolhido o framework Spring por familiaridade e MongoDB pela facilidade.




Exemplos de Request no controlador de linhas de onibus:
    Recuperar todas as linhas de onibus : 
        localhost:8080/buslines
        
        OBS: O metodo HTTP usado foi GET.

    Filtrar linhas de onibus por nome, deve ser passado um nome como parametro de requisicao conforme descrito abaixo:
        localhost:8080/buslines?nome=AEROPORTO

        OBS: O metodo HTTP usado foi GET.

    Adicionar linhas de onibus: 
        localhost:8080/buslines

        OBS: O metodo HTTP usado foi POST.
        OBS2: no corpo da requisicao deve conter os dados da nova de onibus nesse formato: 
        {
            "id": 9999,
            "codigo": "9875",
            "nome": "EXEMPLO"
        }

        
    Atualizar linha de onibus, deve ser passado um id como variavel de rota, abaixo foi usado 9999:
        localhost:8080/buslines/9999

        OBS: O metodo HTTP usado foi PUT.
        OBS2: Tambem deve ser passado no corpo da requisição os novos dados da linhas, conforme abaixo: 
        {
            "codigo": "5005",
            "nome": "SÃO JOSE"
        }   
    
    Deletar linhas de onibus, deve ser passado um id como variavel de rota, abaixo foi usado 9999:
        localhost:8080/buslines/9999

        OBS: O metodo HTTP usado foi DELETE.




Exemplos de Request no controlador de itinerarios:
    Recuperar itinerario, deve ser passado um id como variavel de rota, abaixo foi usado 5516:
        localhost:8080/itineraries/5516

        OBS: O metodo HTTP usado foi GET.

    Pegar linhas de onibus que passam em um determinado raio:
        localhost:8080/itineraries/?lat=-30.003176212571375&longit=-51.1848762570032&radius=3

        OBS: O metodo HTTP usado foi GET.
        OBS2: devem ser passados tres parametros de requisicao, lat (latitude), longit(longitude) e radius (raio), no exemplo foi usado latitude e longitude de uma parte da avenida Sertório.

    Adicionar itinerario:
        localhost:8080/itineraries

        OBS: O metodo HTTP usado foi POST.
        OBS2: Os dados do novo itinerario devem ser passados no corpo da requisicao, conforme abaixo:  
        
        {
            "idlinha": "9999",
            "nome": "NOVA LINHA",
            "codigo": "9666",
            "coordinates": {
                "0": {
                    "lat": -30.003176212571375,
                    "lng": --51.1848762570032
                },
                ...
            }
        }

    Atualizar itinerario : 
        localhost:8080/itineraries/9999

        OBS: O metodo HTTP usado foi PUT.
        OBS2: Os dados do novo itinerario devem ser passados no corpo da requisicao, conforme abaixo:  

        
        {
            "nome": "NOVA LINHA/EXPRESSO",
            "codigo": "9666",
            "coordinates": {
                "0": {
                    "lat": -29.988245103671463,
                    "lng": -51.18126693399819
                },
                ...
            }
        }

        OBS3: A latitude e longitude do exemplo ficam próximos ao aeroporto Salgado Filho.

    Deletar itinerario, deve ser passado um id como variavel de rota : 
        localhost:8080/itineraries/9999
         
        OBS: O metodo HTTP usado foi DELETE.






